import './style/index.less';

import Table from './components/table.vue';
import TableColumn from './components/table-column.vue';
import Grid from './components/grid.vue';

import Bus from './bus.js';

function install (Vue) {
    Vue.component(Table.name, Table);
    Vue.component(TableColumn.name, TableColumn);
    Bus.saveVueRef(Vue);
    Vue.component(Grid.name, Grid);
}

const VueGrid = {
    install
};

export default VueGrid;

export { Grid };

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VueGrid);
}
