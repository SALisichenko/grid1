import 'github-markdown-css';
import 'highlight.js/styles/color-brewer.css';
import 'beautify-scrollbar/dist/index.css';
import './common.less';
import Vue from 'vue';
import VueGrid from './index';
import App from './App';

Vue.use(VueGrid);

new Vue({
    el: '#app',
    render: (h) => h(App)
});
