export function getSortFunction({type, prop, order}) {

    function sortDate(item1, item2) {
        const v1 = new Date(item1[prop]).getTime();
        const v2 = new Date(item2[prop]).getTime();
        const result = v1 < v2 ? -1 : v1 === v2 ? 0 : 1;
        return result===0 ? result : order === 'descending' ? -result :  result;
    }

    function sortDefault(item1, item2) {
        const v1 = item1[prop];
        const v2 = item2[prop];
        const result = v1 < v2 ? -1 : v1 === v2 ? 0 : 1;
        return result===0 ? result : order === 'descending' ? -result : result;
    }
    function sortString(item1, item2) {
        const v1 = item1[prop];
        const v2 = item2[prop];
        const result = v1.localeCompare(v2) ? -1 : v1 === v2 ? 0 : 1;
        return result===0 ? result : order === 'descending' ? -result : result;
    }

    const sortFunctions = {
        date: sortDate,
        string: sortString

    };

    return typeof sortFunctions[type] !== "undefined" ? sortFunctions[type] : sortDefault;
}

