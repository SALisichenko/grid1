// import * as R from 'ramda';
const DAY = 86400000;
export function getFilterFunction({key, filterName, args}) {

    const filterFunctions = {
        numLT: row => row[key] < args[0],
        numGT: row => row[key] > args[0],
        numLTE: row => row[key] <= args[0],
        numGTE: row => row[key] >= args[0],
        numEquals: row => row[key] === args[0],
        numNE: row => row[key] != args[0],
        numIR: row => row[key] > args[0] && row[key] < args[1],

        dateLT: row => row[key] < args[0],
        dateGT: row => row[key] > args[0] + DAY,
        dateLTE: row => row[key] <= args[0] + DAY,
        dateGTE: row => row[key] >= args[0] + DAY,
        dateEquals: row => row[key] >= args[0] && row[key] < args[0] + DAY,
        dateNE: row => row[key] < args[0] && row[key] >= args[1] + DAY,
        dateIR: row => row[key] > args[0] && row[key] < args[1] + DAY,

        strEquals: row => row[key]===args[0],
        strI: row => row[key].indexOf(args[0]) != -1,
        strSW: row => row[key].indexOf(args[0]) === 0,
        strEW: row => row[key].indexOf(args[0], row[key].length - args[0].length -1),

        boolTF: row => row[key] === args[0]
    };

    return filterFunctions[filterName];
}
