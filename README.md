#grid-for-grid
```
Many thanks to dwqs (https://www.npmjs.com/~dwqs) for his table v2-table (https://www.npmjs.com/package/v2-table).
```
-----------------------------------------------------------

## Install:
```
npm install --save grid-for-grid
npm install --save beautify-scrollbar
```
-----------------------------------------------------------

###Global Register:
```
import Vue from 'vue';
import 'beautify-scrollbar/dist/index.css';
import 'grid-for-grid/dist/grid2.css';
import VueGrid from 'grid-for-grid';

Vue.use(VueGrid);
```
-----------------------------------------------------------

###For use:
```
<div>
  <vue-grid :headers="headers" :rows="rows" :options="options"></grid>
</div>

Where:

headers - list with headers's properties: label, prop, width, fixed, align, sortable

Example:

headers:[
{label:'Date', prop:'date'},
{label:'Name', prop:'name', width:'100', fixed:'left'},
{label:'Address', prop:'address', width:'150', align:'right'},
{label:'Birthday', prop:'birthDay'},
{label:'Songs', prop:'songs', width:'100',sortable:true}]

rows - list with rows's properties

Example:

rows:[{
  date: '2017-12-02',
  name: 'test1',
  address: 'Shenzhen,China',
  birthDay: '1988-09-08',
  songs: 3
},
{
  date: '2017-12-027',
  name: 'test2',
  address: 'Shenzhen,China',
  birthDay: '1988-07-05',
  songs: 5
}]

Options - options of table (now is just height)

Example:

options:{
  table_height:600
}
```
